# howto
## If you are me (or pipeline'o'mine)
1. Decrypt with secrets with key  
    ```bash
    gpg .env.gpg
    ```
2. Do everythin like you not me starting from 2nd step.
## If you are not me
1. Define `.env` with this content
    ```ini
    POSTGRES_PASSWORD=mysecretpassword
    PGDATA=/var/lib/postgresql/data/pgdata
    PGADMIN_DEFAULT_EMAIL=example@example.com
    PGADMIN_DEFAULT_PASSWORD=examplepassword
    ```
2. Copy `cfg/nginx.conf` to `~/cfg/nginx.conf` 
3. Copy contents of `src/backend1/` to `~/www/backend1` 
4. Copy contents of `src/backend2/` to `~/www/backend2`
5. Run `docker-compose up -d`


## Notes for reviewers
1. Не совсем понял, что должно выступать точной деплоя, т.к если рассматривать раннер в этом проекте (docker), то деплой будет проиходить в контейнере, а docker in docker сомнительное, но все же зло.
2. GitlabCI вероятно мой самый слабый топик их всех представленных. Есть над чем поработать в общем.
